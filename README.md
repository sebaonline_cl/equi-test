# Test para Equifax #
La estrategia fue montar un autoescaling group de 2 instancias maximo.
De alta disponibilidad con terraform.
## Para eso se creo : ##

aws_elb escuchando el puerto 80,443, y 22.
aws_autoscaling_group en las zonas que se pidio en el diagrama
aws_launch_configuration con el tipo de maquina y la ami.

### Nota ###
Las credenciales del cloud las toma del shared_credentials_file = "$HOME/.aws/credentials"
Hay que hacer un usuario de servicio con permisos de EC2 y loguearse en AWSCLI

## Para bases de datos ##
se creo una instancia de aws_db_instance

## Me falto ##
EL playbook de ansible, no tuve tiempo de configurar un wordpress.
Podira haber elegido una AMI de wordpress, pero no tenia asunto para la prueba.
Si lo que se pedia era trabajo con SCM.

## Por temas de tiempo ... ##
El siguiente paso para ansible, era dejar las instancias con IP elastica.
Las instancias estan asignadas a una ssh .pem
se agregan los host al archivo de ansible (la ip fija del elb)
En el playbook debe ir la conexion a la base de datos con el endpoint que dio aws_db_instance